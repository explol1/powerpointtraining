FROM node:20-alpine
RUN apk add --no-cache git
RUN git clone https://gitlab.com/juanluisriveramillan4/BusinessManage.git
WORKDIR /BusinessManage
RUN npm install
CMD npm start